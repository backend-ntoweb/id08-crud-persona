package app.nike.id08crudpersona.models;

import lombok.Data;

@Data
public class Lavoro {
	private long id;
	private String nomeAzienda;
	private String data;

	private String tipo;
	private String contratto;
	private int livello = 0;
	private long ral = 0;
	private long tariffaGiornalieraNetta = 0;
	private String specificaContratto;
	private String nomeReferente;
		
}
