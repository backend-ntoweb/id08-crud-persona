package app.nike.id08crudpersona.models;

import lombok.Data;

@Data
public class Diploma {

	private long id;

	private String nomeDiploma;

	private String annoConseguimento;

}
