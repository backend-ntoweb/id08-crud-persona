package app.nike.id08crudpersona.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.nike.id08crudpersona.models.Persona;
import app.nike.id08crudpersona.repositories.PersonaRepository;

@Service
public class PersonaServiceImpl implements PersonaServiceInterface {

    /*
     * Classe dedicata alle Crud che riguardano l'entità persona
     */

    @Autowired
    private PersonaRepository personaRepository;

    /**
     *
     * @param p
     * @return
     * @throws Exception
     */
    @Override
    public Persona addPersona(Persona p) throws Exception {
        Persona persona = personaRepository.findByCodiceFiscale(p.getCodiceFiscale());
        try {
            if (persona != null)
                throw new Exception("persona già registrata");
            else if (p.getCodiceFiscale() == null)
                throw new Exception("codice fiscale non inserito");

            return personaRepository.save(p);
        } catch (NullPointerException e) {
            throw new NullPointerException("Persona inviata vuota");
        } catch (Exception e) {
            Exception ex = new Exception(e.getMessage());
            throw ex;
        }
    }

    /**
     *
     * @param p
     * @return
     */
    @Override
    public Persona updatePersona(Persona p) throws Exception {

        try {
            if (p == null) {
                throw new NullPointerException("persona da aggiornare vuota");
            }
            if ((personaRepository.findByCodiceFiscale(p.getCodiceFiscale())) == null) {
                throw new Exception("persona da aggiornare inesistente");
            }

            Persona personaDaAggiornare = personaRepository.findByCodiceFiscale(p.getCodiceFiscale());
            p.setId(personaDaAggiornare.getId());

            return personaRepository.save(p);
        } catch (Exception e) {
            Exception ex = new Exception(e.getMessage());
            throw ex;
        }
    }

    /**
     *
     * @param codiceFiscale
     */
    @Override
    public void deletePersona(String codiceFiscale) throws Exception {

        Persona p = personaRepository.findByCodiceFiscale(codiceFiscale);

        try {
            if (p == null) {
                throw new Exception("persona da cancellare non trovata");
            }
            personaRepository.delete(p);
        } catch (Exception e) {
            Exception ex = new Exception(e.getMessage());
            throw ex;
        }
    }

    /**
     *
     * @param codiceFiscale
     * @return
     */
    @Override
    public Persona getPersona(String codiceFiscale) throws Exception {

        Persona p = personaRepository.findByCodiceFiscale(codiceFiscale);
        try {

            if (p == null) {
                throw new Exception("persona non trovata");
            }
            return p;
        } catch (Exception e) {
            Exception ex = new Exception(e.getMessage());
            throw ex;
        }
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<Persona> getPersone() throws Exception {
        try {
            return personaRepository.findAll();
        } catch (Exception e) {
            throw new Exception("il server non è riuscito a recuperare la lista di persone");
        }
    }

}
