package app.nike.id08crudpersona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Id08CrudPersonaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Id08CrudPersonaApplication.class, args);
    }

}
